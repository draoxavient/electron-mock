const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
app.use(cors());
// app.use(bodyParser());
// Body Parser Middleware
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

global.count = 0;
const timeout = 10000;

// middleware for 5 sec delay in each response.
app.use((req, res, next) => setTimeout(() => next(), timeout));

// mock of bar-code reader device
app.get('/readBarcode', (req, res) => {
	if (global.count >= 2) return res.json({ value: '' });
	const sid = ['076750043900009877890' + (Math.floor(Math.random() * 90000) + 100000000), '076750046690009877890000000000'];
	console.log('sid', sid[global.count]);
	res.json({ value: sid[global.count++] });
});

app.get('/abortReadBarcode', (req, res) => res.json({}));

// mock of cash device
app.get('/accepterStatus', (req, res) => res.json({ result: 'ok', deviceState: 'Idling' }));
app.get('/stackCash', (req, res) => res.json({ result: 'ok' }));
app.get('/abortAcceptCash', (req, res) => res.json({ result: 'ok' }));

app.get('/acceptCash', (req, res) => {
	const amount = 25 * ++global.count;
	const d = {
		amountReceived: amount,
		status: 'completed',
		mode: 'cash',
		result: 'ok',
		value: [amount]
	};
	setTimeout(() => res.send(d), 2000);
});

app.get('/rejectCash', (req, res) => res.json({}));

// mock of credit/debit card device

app.post('/payment/preauth', (req, res) => res.json({ transactionId: '26c4b87e-b11f-495f-b93b-be6a0afebef3' }));
app.post('/payment/postauth', (req, res) => res.json({ transactionId: '26c4b87e-b11f-495f-b93b-be6a0afebef3' }));
app.get('/payment/status', (req, res) => {
	res.json({
		status: 'Completed',
		preauthResult: {
			card: {
				account: 'XXXXXXXXXXXX0026',
				exp: '0420',
				cardType: 'VISA',
				cardIssueType: 'CREDIT',
				entryMode: 'MSR'
			},
			gatewayResponse: {
				transactionMode: 'Credit',
				transactionResult: 'Approved',
				authDate: '2018-04-04T11:45:58.2684408Z',
				authAmount: 500,
				authCode: '3IHAH6'
			},
			transactionId: req.query.transactionId,
			result: 'Success'
		},
		type: 'Preauth'
	});
});

app.post('/payment/status/list', (req, res) => res.json({ status: 'completed', transactionId: '26c4b87e-b11f-495f-b93b-be6a0afebef3' }));
app.post('/payment/void', (req, res) => res.json({ status: 'completed', transactionId: '26c4b87e-b11f-495f-b93b-be6a0afebef3' }));
app.get('/receiptPrinterStatus', (req, res) => res.json({ errorMessage: 'Print receipt fail!' }));

app.get('/cardPrinterStatus', (req, res) => res.json({ printerReady: true }));
app.post('/printCard', (req, res) => res.json({ success: 'true' }));
app.post('/printReceipt', (req, res) => res.json({ errorMessage: 'Print receipt fail!' }));

// mock of card injector device.
app.get('/ingestCard', (req, res) => {
	const d = { ingestedCard: true };
	setTimeout(() => res.send(d), 5000);
});

app.get('/cancelIngestCard', (req, res) => {
	global.count = 0;
	res.json({ canceled: true });
});

app.listen(8072, () => console.log('Example app listening on port 8072!'));

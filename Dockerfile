FROM node:8.12.0-alpine
WORKDIR /app
COPY package.json /app
RUN yarn
COPY . /app
CMD node /app/index.js
EXPOSE 8072

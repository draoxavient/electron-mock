

# Electron Mock

 this project is used to mock services of payment device, bar-code reader , card-injector & other devices serve locally on kiosk machine

## Installation

```bash
npm install 
```

## Get started

```bash
npm start
```
## Pre configuration :

Open kiosk.json file, change following configuration :

```bash
 "devices": {
        "printer": {
            "base": "http://localhost:8072"
        },
        "payment": {
            "base": "http://localhost:8072",
            "timeout": 120000
        },
        "scanner": {
            "base": "http://localhost:8072"
        },
        "receipt": {
            "base": "http://localhost:8072"
        },
        "cashCollector": {
            "base": "http://localhost:8072"
        },
        "ingestCard": {
            "base": "http://localhost:8072"
        }
    },
```
